
/**
 * Run the app
 *
 */
$(document).ready(function () {
    // https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions
    var options = {
        enableHighAccuracy: false,
        timeout: 10000,
        maximumAge: 500000
    };
    var positionWatcher;

    var debug = function(message) {
        if ($('#enable-debug').is(':checked')) {
            $('#debug pre').prepend(message+"\n");
        }
    }
    $('#clear-debug').click(function(e){
        $('#debug pre').html('');
    });

    var updatePos = function(position) {
        // console.log(position);

        // Update info
        $('#latitude').html(position.coords.latitude);
        $('#longitude').html(position.coords.longitude);
        $('#accuracy').html(position.coords.accuracy);
        var date = new Date(position.timestamp);
        $('#date').html(date.toLocaleString());

        // Debug info
        if (typeof position.coords.accuracy === 'undefined') position.coords.accuracy = 0;
        debug("[log] "+date.toLocaleString()+" | "+position.coords.latitude+", "+position.coords.longitude+" ("+position.coords.accuracy+")");

        // Update server
        $.ajax({
            type: "POST",
            crossdomain: true,
            url: $('#server-url').val(),
            data: {'position' : position},
            success: function(data, textStatus, jqXHR){
                if (data == 'success') {
                    debug("[success] Server successfully get position.");
                }
                else {
                    debug("[error] Something when wrong when saving position.");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                debug("[error] Server error: "+errorThrown);
            },
            dataType: 'json'
        });
    };

    var errorPos = function(error){
        // https://developer.mozilla.org/fr/docs/Utilisation_de_la_g%C3%A9olocalisation#Traitement_d'erreur
        debug("[error] "+error.code+" --- "+error.message);
    }

    // Manual tracking
    $('#get').click(function(e){
        navigator.geolocation.getCurrentPosition(updatePos, errorPos, options);
    });

    // Automatic tracking
    $('#track').click(function(e){
        // Debug message
        debug("[status] Start watching position.");

        // Toggle button status
        $('#track').attr('disabled', 'disabled');
        $('#stop').removeAttr('disabled');

        // Start watching position
        positionWatcher = navigator.geolocation.watchPosition(updatePos, errorPos, options);
    });
    $('#stop').click(function(e){
        // Debug message
        debug("[status] Stop watching position.");

        // Toggle button status
        $('#stop').attr('disabled', 'disabled');
        $('#track').removeAttr('disabled');

        // Stop watching position
        navigator.geolocation.clearWatch(positionWatcher);
    });

    // Open with gmap
    $('#gmap').click(function(e){
        if ($('#latitude').html() !== "n/a") {
            var lat = $('#latitude').html(),
            lon = $('#longitude').html();
            window.open("https://maps.google.com/maps?daddr="+lat+","+lon+"&ll="+lat+","+lon+"&q="+lat+","+lon+"&z=8");
        }
        else {
            alert("Error: No position available.");
        }
    });

});
