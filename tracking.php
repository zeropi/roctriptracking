<?php

// See config.php.sample
include 'config.php';

function error($message) {
  header('Content-Type: application/json');
  header("HTTP/1.1 500 Internal Server Error");
  print json_encode($message);
  exit;
}
function success($message) {
  header('Content-Type: application/json');
  print json_encode($message);
  exit;
}

if (!empty($_POST['position'])) {
  // Get position
  $position = $_POST['position'];

  // Do your processing here.
  if ($dbhandle = mysql_connect($database['host'], $database['user'], $database['pass'])) {
    if ($db = mysql_select_db($database['database'], $dbhandle)) {
      // We're connected !

      // Validate position
      $latitude = (float) $position['coords']['latitude'];
      $longitude = (float) $position['coords']['longitude'];
      $precision = (!empty($position['coords']['accuracy'])) ? (int) $position['coords']['accuracy'] : 0;
      $timestamp = (int) $position['timestamp'];

      if ($latitude == 0 || $longitude == 0) {
        error("Wrong coordinates");
      }

      // Insert position
      // INSERT INTO `position` (`id`,`latitude`,`longitude`,`time`,`precision`,`lat`) VALUES ('','','','','','');
      $result = mysql_query("
        INSERT INTO `position` (`latitude`,`longitude`,`time`,`precision`)
          VALUES (".$latitude.",".$longitude.",".$timestamp.",".$precision.");
      ");
      mysql_close($dbhandle);

      success("success");
    }
    else {
     error("Could not select database");
    }
  }
  else {
    error("Unable to connect to MySQL");
  }
}

// Something went wrong
error("Internal Server Error");